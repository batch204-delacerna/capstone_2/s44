const Users = require("../models/Users");
const Products = require("../models/Products");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration
module.exports.userRegistration = (requestBody) => {

	// Validation for existing users
	return Users.findOne({email: requestBody.email}).then(result => {

		if (result !== null) {
			
			//return false //For REACT.js
			return "User email already exists!";

		} else {

			let newUser = Users({
				email: requestBody.email,
				password: bcrypt.hashSync(requestBody.password, 10),
				isAdmin: requestBody.isAdmin
			});

			return newUser.save().then((user, error) => {

				if (error) {
					//return false //For REACT.js
					console.log(error);
					return "User registration failed!";
				} else {
					// return true //For REACT.js
					return `User registration successful! User ${newUser.email} created!`;
				}


			});
		}

	});

}

// User Authentication
module.exports.userLogin = (requestBody) => {

	return Users.findOne({email: requestBody.email}).then(result => {

		if (result == null) {
			return "User does not exist, please signup first.";
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return "Incorrect password."
			}

		}

	});

} 